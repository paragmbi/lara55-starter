<?php

Route::get('/home', function () {
    /**
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return view('admin.home');
    /**/
    return redirect(route('admin.dashboard'));
})->name('home');

Route::get('/dashboard', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return view('admin.dashboard');
})->name('dashboard');

Route::get('/template', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return view('admin.template');
})->name('template');

Route::get('/invoice',function(){
	return view('admin.invoice');
})->name('invoice');

Route::get('/print-invoice',function(){
	/**
	$pdf = PDF::loadView('admin.orders.invoice',compact('data','data1','data2','Order_Code','Visitor'));
	return $pdf->download('OrderInvoice-'.$Order_Code.'.pdf');
	/**/
	$pdf = PDF::loadView('admin.invoice');
	return $pdf->download('OrderInvoice_dated_'.date('d-m-Y').'.pdf');
})->name('print-invoice');
