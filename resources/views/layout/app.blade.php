<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .active{
                font-weight: 600;
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('user.login'))
                <div class="top-right links">
                    @if(Auth::guard('admin')->check())
                        <a href="{{ route('admin.home') }}" target="_blank">Admin Panel</a>
                    @endif
                    <a href="{{ route('home') }}"><span class="{{ (Route::current()->getName() == 'home') ? 'active' : '' }}">Home</span></a>
                    <a href="{{ route('about') }}"><span class="{{ (Route::current()->getName() == 'about') || Request::is('about/*') ? 'active' : '' }}">About</span></a>
                    <a href="{{ route('contact-us') }}"><span class="{{ (Route::current()->getName() == 'contact-us') ? 'active' : '' }}">Contact Us</span></a>
                    @if(Auth::guard('user')->check())
                        <a href="{{ route('user.home') }}">My Account</a>
                    @else
                        <a href="{{ route('user.login') }}">Login</a>
                        <a href="{{ route('user.register') }}">Register</a>
                    @endif
                </div>
            @endif

            @yield('content')

        </div>
    </body>
</html>