@extends('admin.layout.auth')

@section('meta-title')
Log in | {{ Config::get('app.app_name') }}
@endsection

<!-- Main Content -->
@section('content')
<div class="login-box">
  <div class="login-logo">
    <b>{{ Config::get('app.app_name') }}</b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form role="form" method="POST" action="{{ url('/admin/login') }}">
      {{ csrf_field() }}
      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>
      <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
        <input id="password" type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember">&emsp;Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-success btn-block btn-flat"><i class="fa fa-sign-in fa-fw"></i> Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <p><strong>username: <u>admin@admin.com</u><br>password: <u>123456</u></strong></p>
    <a href="{{ url('/admin/password/reset') }}"><i class="fa fa-key fa-fw"></i> I forgot my password</a><br>
    <a href="{{ route('admin.register') }}" class="text-center"><i class="fa fa-plus fa-fw"></i> Register a new membership</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@endsection
