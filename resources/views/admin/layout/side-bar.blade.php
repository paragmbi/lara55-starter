<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ asset('assets/admin/img/profile-pic/'.Auth::user()->profile_pic) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->name }}</p>
              <a href="#" class="no-link"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ (Route::current()->getName() == 'admin.dashboard') ? 'active' : 'no-active' }}">
              <a href="{{ route('admin.dashboard') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-share"></i> <span>Multilevel</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level One
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li class="treeview">
                      <a href="#"><i class="fa fa-circle-o"></i> Level Two
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
              </ul>
            </li>
            <li class="{{ (Route::current()->getName() == 'admin.invoice') ? 'active' : 'no-active' }}"><a href="{{ route('admin.invoice') }}"><i class="fa fa-file-pdf-o"></i> Invoice</a></li>
			<li class="{{ (Route::current()->getName() == 'admin.print-invoice') ? 'active' : 'no-active' }}"><a href="{{ route('admin.print-invoice') }}"><i class="fa fa-print"></i> Print Invoice</a></li>
            <li class="{{ (Route::current()->getName() == 'admin.template') ? 'active' : 'no-active' }}"><a href="{{ route('admin.template') }}"><i class="fa fa-info-circle"></i> Template</a></li>
            <li><a href="#" class="no-link"><i class="fa fa-info-circle"></i> 400 Bad Request</a></li>
            <li><a href="#" class="no-link"><i class="fa fa-info-circle"></i> 401 Unauthorized</a></li>
            <li class="header">Version: {{ Config::get('app.app_version') }}</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>