<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('meta-title')</title>
  <meta name="description" content="@yield('meta-description')">
  <meta name="keywords" content="@yield('meta-keywords')">
  <meta name="base-url" content="{{ Config::get('app.url') }}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('assets/admin/components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets/admin/components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('assets/admin/components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/admin/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('assets/admin/css/skins/_all-skins.min.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesnot work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <!-- Custom Stylesheet -->
  <link rel="stylesheet" href="{{ asset('assets/admin/css/custom.css') }}">
</head>
<body class="hold-transition skin-blue fixed">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Top Header -->
  @include('admin.layout.top-bar')

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  @include('admin.layout.side-bar')

  @yield('content')

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      Crafted By <b><a href="{{ Config::get('app.author_url') }}" target="_blank" title="{{ Config::get('app.author_name') }}">{{ Config::get('app.author_keyword') }}</a></b>
    </div>
  <strong><i class="fa fa-copyright fa-fw"></i> {{ date('Y') }} <a href="{{ Config::get('app.url') }}" target="_blank">{{ Config::get('app.app_name') }}</a>.</strong> All rights
    reserved.
  </footer>

  <?php /** ?>
  <!-- Control Sidebar -->
  @include('admin.layout.control-side-bar')
  <?php /**/ ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('assets/admin/components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/admin/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Custom Script -->
<script src="{{ asset('assets/admin/js/custom.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('assets/admin/components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/admin/components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/admin/js/demo.js') }}"></script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
</body>
</html>